" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

"My configs
set number
set smarttab
set shiftwidth=4
set tabstop=4
set expandtab
set autoindent
set smartindent
set ai
set mouse=a
set incsearch

autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``
autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,clas

set termencoding=utf-8
set novisualbell
set nobackup
set noswapfile

set encoding=utf-8
set fileencodings=utf-8,cp1251
set ruler
set hlsearch
set cursorline

map <C-e> :noh<return>

noremap <C-C> "+y

set wrap
set showbreak=__

augroup vimrc_autocmds
    autocmd!
    autocmd FileType ruby,python,javascript,c,cpp highlight Excess ctermbg=DarkGrey guibg=Black
    autocmd FileType ruby,python,javascript,c,cpp match Excess /\%99v.*/
    autocmd FileType python,rst,c,cpp set colorcolumn=99
augroup END

syntax on

" Plugins

call plug#begin('~/.local/share/nvim/plugged')

Plug 'davidhalter/jedi-vim'
" if has('nvim')
"  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" else
"  Plug 'Shougo/deoplete.nvim'
"  Plug 'roxma/nvim-yarp'
"  Plug 'roxma/vim-hug-neovim-rpc'
" endif
Plug 'itchyny/lightline.vim'
Plug 'dense-analysis/ale'

Plug 'preservim/nerdtree' |
            \ Plug 'Xuyuanp/nerdtree-git-plugin' |
            \ Plug 'ryanoasis/vim-devicons' |
            \ Plug 'ryanoasis/nerd-fonts' |
            \ Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }

Plug 'Townk/vim-autoclose'

Plug 'mhinz/vim-signify'
Plug 'itchyny/vim-gitbranch'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'

Plug 'psliwka/vim-smoothie'
Plug 'neomake/neomake'
Plug 'majutsushi/tagbar'

Plug 'joshdick/onedark.vim'
Plug 'mhartington/oceanic-next'
Plug 'frazrepo/vim-rainbow'
Plug 'pechorin/any-jump.vim'
Plug 'vim-python/python-syntax'

call plug#end()


" NERDTree settings
nnoremap <C-o> :NERDTreeToggle<CR>
let g:NERDTreeGitStatusUseNerdFonts = 1

" FZF settings
nnoremap <C-f> :FZF<CR>

" git settings
nnoremap <C-g> :GV<CR>
let g:lightline = {
      \ 'colorscheme': 'oceanicnext',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }

" colorscheme settings
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
if (has("termguicolors"))
 set termguicolors
endif
let g:oceanic_next_terminal_bold = 1
let g:oceanic_next_terminal_italic = 1
colorscheme OceanicNext

" code checking
let g:neomake_python_enabled_makers = ['pylint']
call neomake#configure#automake('nrwi', 500)

" tagbar settings
let g:tagbar_autofocus=0
let g:tagbar_width=42
autocmd BufEnter *.py :call tagbar#autoopen(0)

" Rainbow settings
let g:rainbow_active = 1

" AnyJump settings
nnoremap <C-j> :AnyJump<CR>
nnoremap <C-p> :AnyJumpBack<CR>

" Python-syntax settings
let g:python_highlight_all = 1

"Jedi settings
set completeopt=menu,menuone
